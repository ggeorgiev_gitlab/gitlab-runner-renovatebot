Object.assign(process.env, {
    GIT_AUTHOR_NAME: 'Renovate Bot',
    GIT_AUTHOR_EMAIL: 'gitlab-runner-renovate-bot@gitlab.com',
    GIT_COMMITTER_NAME: 'Renovate Bot',
    GIT_COMMITTER_EMAIL: 'gitlab-runner-renovate-bot@gitlab.com'
});

module.exports = {
    endpoint: process.env.CI_API_V4_URL,
    platform: 'gitlab',
    username: 'gitlab-runner-renovatebot',
    gitAuthor: `${process.env.GIT_AUTHOR_NAME} <${process.env.GIT_AUTHOR_EMAIL}>`,
    hostRules: [
        {
            hostType: 'docker',
            username: process.env.DOCKER_HUB_USERNAME,
            password: process.env.DOCKER_HUB_PASSWORD
        }
    ]
};
